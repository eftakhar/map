angular.module('starter.controllers', ['uiGmapgoogle-maps'])
.run(['$templateCache', function ($templateCache) {
  $templateCache.put('currentlocation.tpl.html', '<img src="img/current.png" ng-click="controlClick()">{{controlText}}</button>');
}])

.controller('controlController', function ($scope,$cordovaGeolocation,$rootScope) {
  $scope.controlText = 'I\'m a custom control';

  $scope.danger = false;
  $scope.controlClick = function () {var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };
    $cordovaGeolocation.getCurrentPosition(options).then(function (pos) {
      alert("inside");
      console.log('Got pos', pos);
      $rootScope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
    alert('custom control clicked!')
  };
})
.factory('$cordovaGeolocation', ['$q', function ($q) {

    return {
      getCurrentPosition: function (options) {
        var q = $q.defer();

        navigator.geolocation.getCurrentPosition(function (result) {
          // Do any magic you need
          console.log("current");
          q.resolve(result);
        }, function (err) {
          alert("error factory");
          q.reject(err);
        }, options);

        return q.promise;
      },
      watchPosition: function (options) {
        var q = $q.defer();

        var watchId = navigator.geolocation.watchPosition(function (result) {
          // Do any magic you need
          q.notify(result);

        }, function (err) {
          q.reject(err);
        }, options);

        return {
          watchId: watchId,
          promise: q.promise
        }
      },

      clearWatch: function (watchID) {
        return navigator.geolocation.clearWatch(watchID);
      }
    }
  }])
.controller('DashCtrl', function ($scope,uiGmapGoogleMapApi,$timeout,$interval,$cordovaGeolocation,$rootScope) {
  $rootScope.currentlocation=function(){
      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };
      
      $cordovaGeolocation.getCurrentPosition(options).then(function (pos) {
        latlong =  { 'lat' : pos.coords.latitude, 'long' : pos.coords.longitude }
        $rootScope.currentLocation = latlong;
        console.log("x"+pos);
        $scope.map={center: { latitude: pos.coords.latitude, longitude: pos.coords.longitude }}
      }, function(err) {
        console.log("error");
      });
    };
   uiGmapGoogleMapApi.then(function(maps) {
    $scope.googleVersion = maps.version;
    maps.visualRefresh = true;

    //$scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 16 };

    });

   angular.extend($rootScope, {
    map: {

      show: true,
      control: {},
      version: "uknown",
      mapTypeId :"roadmap",
      showTraffic: true,
      showBicycling: false,
      showWeather: false,
      showHeat: false,
      center: {
        latitude: 12,
        longitude: 77
      },
      options: {
        streetViewControl: false,
        panControl: false,
        maxZoom: 20,
        minZoom: 3
      },
      zoom: 8,
      dragging: false,
      bounds: {},
      
      markers: [
        {
          id: 1,
          icon: 'img/blue_marker.png',
          latitude: 12,
          longitude: 77,
          showWindow: false,
          options: {
            labelContent: 'DRAG ME!',
            labelAnchor: "22 0",
            labelClass: "marker-labels",
            draggable: true
          }
        }        
      ]
      }     
     
    

   });
$scope.map.markers2Events = {
    dragend: function (marker, eventName, model, args) {
      model.options.labelContent = "Dragged lat: " + model.latitude + " lon: " + model.longitude;
      console.log("Dragged lat: " + model.latitude + " lon: " + model.longitude);
      var geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(model.latitude, model.longitude);
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            //element.text(results[1].formatted_address);
                            console.log(results);
                            $scope.address=results[0].formatted_address;
                            console.log(results[0].formatted_address)
                        } else {
                            //element.text('Location not found');
                            console.log('Location not found')
                        }
                    } else {
                        //element.text('Geocoder failed due to: ' + status);
                        console.log('Geocoder failed due to: ' + status);

                    }
                });

    }
  };

})






